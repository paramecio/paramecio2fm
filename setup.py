#!/usr/bin/env python3

import sys
import os
from setuptools import setup, find_packages


if sys.version_info < (3, 8):
    raise NotImplementedError("Sorry, you need at least Python 3.8 for use paramecio2.")

#import paramecio
# Pillow should be installed after if you need ImageField
# If you install passlib and bcrypt, the password system will use bcrypt by default, if not, will use native crypt libc 

setup(name='paramecio2',
      version='2.0.27',
      description='Simple Web Framework based in flask and Mako.',
      long_description='This framework is a simple framework used for create web apps. Paramecio is modular and fast. By default have a module called admin that can be used for create admin sites',
      author='Antonio de la Rosa Caballero',
      author_email='antonio.delarosa@coesinfo.com',
      url='https://bitbucket.org/paramecio/paramecio2fm/',
      packages=['paramecio2'],
      include_package_data=True,
      install_requires=['flask', 'pymysql', 'sqlalchemy', 'colorama', 'python-slugify', 'mako', 'pillow', 'arrow', 'beautifulsoup4'],
      entry_points={'console_scripts': [
        'paramecio2 = paramecio2.console:start', 'paramecio2db = paramecio2.libraries.db.dbadmin:start', 'paramecio2lang = paramecio2.libraries.check_i18n:start',
      ]},
      zip_safe=False,
      license='GPLV3',
      platforms = 'any',
      classifiers=['Development Status :: 1 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLV3 License',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: CGI Tools/Libraries',
        'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
        'Topic :: Internet :: WWW/HTTP :: WSGI',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Server',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8'
        'Programming Language :: Python :: 3.9'
        'Programming Language :: Python :: 3.10'
        'Programming Language :: Python :: 3.11'
        ],
     )
