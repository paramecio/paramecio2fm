from settings import config
from flask import g, url_for
from paramecio2.libraries.i18n import I18n
import copy
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.modules.login.models.login import LoginType
from paramecio2.libraries.db import coreforms
from paramecio2.libraries.formsutils import show_form
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
import os

#env=env_theme(__file__)

#t=PTemplate(env)

t=copy.copy(admin_t)

t.env.directories.insert(1, os.path.dirname(__file__)+'/templates')

#print(t.env.directories)

@admin_app.route('/admin/admin_login/', methods=['GET'])
def admin_login():
    
    connection=g.connection
    """
    login_type=LoginType(connection)
    
    url=url_for('.admin_login')
    
    admin=GenerateAdminClass(login_type, url, t)
    
    login_type.create_forms()
    
    login_type.forms['double_check'].arr_select={0: I18n.lang('admin', 'no', 'No'), 1: I18n.lang('admin', 'yes', 'Yes')}
    """
    """
    user_admin=UserAdmin(connection)
    
    user_admin.fields['privileges'].name_form=SelectForm
    
    user_admin.create_forms(['username', 'password', 'email', 'privileges', 'lang'])
    
    user_admin.forms['privileges'].arr_select={0: I18n.lang('admin', 'without_privileges', 'Without privileges'), 1: I18n.lang('admin', 'selected_privileges', 'Selected privileges'), 2: I18n.lang('admin', 'administrator', 'Administrator')}
    
    user_admin.fields['password'].protected=False
    
    user_admin.check_user=False
    user_admin.check_email=False
    
    url=url_for('admin_app.login')
    
    admin=GenerateAdminClass(user_admin, url, t)
    
    admin.list.fields_showed=['username', 'privileges']
    
    admin.list.search_fields=['username']
    
    admin.arr_fields_edit=['username', 'password', 'repeat_password', 'email', 'privileges', 'lang']
    """
    """
    form_admin=admin.show()
    
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('admin', 'users_edit', 'Users edit'), contents=form_admin, path_module='/admin/login')    
    else:
        
        return form_admin    
    """
    """
    form_admin=''
    
    arr_forms={}
    
    arr_forms['name']=coreforms.BaseForm('name', '')
    
    arr_forms['name'].label='Name'
    
    arr_forms['activate_double_check']=coreforms.SelectForm('activate_double_check', 0, {0: I18n.lang('admin', 'no', 'No'), 1: I18n.lang('admin', 'yes', 'Yes')})
    
    arr_forms['activate_double_check'].label='Activate double check'
    
    form_admin=show_form({}, arr_forms, t, yes_error=True, pass_values=True, modelform_tpl='forms/modelform.phtml')
    
    login_form=t.load_template('loginform.phtml', content_form=form_admin)
    
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('admin', 'login_edit', 'Login edit'), contents=login_form, path_module='/admin/login')    
    else:
        
        return form_admin    

    """
    
    return ""
"""
@admin_app.route('/admin/login/', methods=['POST'])
def admin_login_post():
    
    data={'error': 0, 'txt_error': 0}
    
    # Save in json
    
    
    
    return data
    
    
"""
