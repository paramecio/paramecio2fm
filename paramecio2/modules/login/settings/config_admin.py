from paramecio2.libraries.config_admin import config_admin
from paramecio2.libraries.i18n import I18n

#modules_admin=[[I18n.lang('admin', 'users_admin', 'User\'s Admin'), 'paramecio.modules.admin.admin.ausers', 'ausers']]

config_admin.append([I18n.lang('admin', 'login_admin', 'Login admin')])

config_admin.append([I18n.lang('admin', 'login_config', 'Login configuration'), 'paramecio2.modules.login.admin.login', 'admin_app.admin_login'])

