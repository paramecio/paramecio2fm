#!/usr/bin/env python3

from paramecio2.libraries.i18n import I18n
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db.usermodel import UserModel
from paramecio2.libraries.db import corefields

class LoginType(WebModel):
    
    def __init__(self, connection=None):

        super().__init__(connection)
        
        self.register(corefields.CharField('name_login'), True)
        self.register(corefields.BooleanField('double_check'))
