from flask import Blueprint, g, request, session, redirect, url_for
from paramecio2.libraries.db.webmodel import WebModel
from functools import wraps
from paramecio2.libraries.mtemplates import PTemplate, env_theme
try:
    import ujson as json
except:
    import json
    
#Load json config

login_app=Blueprint('login_app', __name__)

env=env_theme(__file__)

t=PTemplate(env)

def login_site(f):
    
    @wraps(f)
    
    def wrapper(*args, **kwds):
        
        session_name='login_site'
        
        if 'sesion_name' in kwds:
            session_name=kwds['session_name']
        
        if not session_name in session:
            
            return redirect(url_for('.login'))
            
        else:
            
            return f(*args, **kwds)
   
    return wrapper
