
from settings import config
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.modules.welcome import welcome_app

env=env_theme(__file__)

t=PTemplate(env)

@welcome_app.route('/welcome')
def home():

    return t.load_template('welcome.phtml', title="Welcome", content='Welcome to the real world')

    #return render_template('welcome.html', title="Welcome")
"""
@welcome_app.route('/welcome/redirect')
def welcome_redirect():
    
    return redirect('/welcome')
"""

if config.default_module=="welcome":

    home=welcome_app.route("/")(home)
