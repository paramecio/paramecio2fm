try:
    from flask import Blueprint
    from paramecio2.libraries.mtemplates import PTemplate, env_theme

    admin_app=Blueprint('admin_app', __name__, static_folder='static')


    env=env_theme(__file__)

    t=PTemplate(env)
    
except:
    pass
