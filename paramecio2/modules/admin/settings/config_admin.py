from paramecio2.libraries.config_admin import config_admin
from paramecio2.libraries.i18n import I18n

#modules_admin=[[I18n.lang('admin', 'users_admin', 'User\'s Admin'), 'paramecio.modules.admin.admin.ausers', 'ausers']]

config_admin.append([I18n.lang('admin', 'users', 'Users')])

config_admin.append([I18n.lang('admin', 'users_edit', 'Users edit'), 'paramecio2.modules.admin.admin.ausers', 'admin_app.ausers', 'fa-user'])
