from settings import config
from flask import g, url_for
from paramecio2.modules.admin.models.admin import UserAdmin
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.libraries.db.coreforms import SelectForm
from paramecio2.libraries.db.coreforms import HiddenForm
import copy
from paramecio2.modules.admin import admin_app, t as admin_t

t=copy.copy(admin_t)

@admin_app.route('/admin/ausers/', methods=['GET', 'POST'])
def ausers():
    
    connection=g.connection
    
    user_admin=UserAdmin(connection)
    
    user_admin.fields['privileges'].name_form=SelectForm
    
    user_admin.fields['disabled'].name_form=SelectForm
    
    user_admin.fields['double_auth'].name_form=SelectForm
    
    user_admin.fields['last_login'].name_form=HiddenForm
    
    user_admin.create_forms(['username', 'password', 'email', 'privileges', 'lang', 'disabled', 'double_auth', 'last_login'])
    
    user_admin.forms['privileges'].arr_select={0: I18n.lang('admin', 'without_privileges', 'Without privileges'), 1: I18n.lang('admin', 'selected_privileges', 'Selected privileges'), 2: I18n.lang('admin', 'administrator', 'Administrator')}
    
    user_admin.forms['disabled'].arr_select={0: I18n.lang('admin', 'user_enabled', 'User enabled'), 1: I18n.lang('admin', 'user_disabled', 'User disabled')}
    
    user_admin.forms['double_auth'].arr_select={0: I18n.lang('admin', 'no', 'No'), 1: I18n.lang('admin', 'yes', 'Yes')}
    
    user_admin.fields['password'].protected=False
    
    user_admin.check_user=False
    user_admin.check_email=False
    
    url=url_for('admin_app.ausers')
    
    admin=GenerateAdminClass(user_admin, url, t)
    
    admin.list.fields_showed=['username']
    
    admin.list.search_fields=['username']
    
    admin.arr_fields_edit=['username', 'password', 'repeat_password', 'email', 'lang', 'double_auth', 'disabled', 'last_login']
    
    form_admin=admin.show()
    
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('admin', 'users_edit', 'Users edit'), contents=form_admin, path_module='admin_app.ausers')    
    else:
        
        return form_admin    

