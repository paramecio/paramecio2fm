import sys
import os
import pytest

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../')

#from settings import config
from paramecio2.libraries.db.webmodel import PhangoField, WebModel
from paramecio2.libraries.db import corefields
from paramecio2.libraries.db.extrafields.arrayfield import ArrayField
from paramecio2.libraries.db.extrafields.colorfield import ColorField
from paramecio2.libraries.db.extrafields.datefield import DateField
from paramecio2.libraries.db.extrafields.datetimefield import DateTimeField
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.emailfield import EmailField
from paramecio2.libraries import datetime

class ExampleModel(WebModel):
    
    def __init__(self, connection=None):

        super().__init__(connection)

        # I can change other fields here, how the name.

        self.register(corefields.CharField('title'))
        self.register(corefields.CharField('content'))


def test_test_phangofield():
    
    field=PhangoField('default', 255)

    assert field.check(' "<trial>" ')=='&quot;&lt;trial&gt;&quot;'

def test_test_integerfield():
    
    field=corefields.IntegerField('int')
    
    assert field.check("25")=='25'
    
    assert field.check("25j")=='0'
    
    field.check("25j")
    
    assert field.error==True

def test_test_floadfield():
    
    field=corefields.FloatField('int')
    
    assert field.check('1,5')=='1.5'
    
    assert field.check('1.5456')=='1.5456'
    
    assert field.check('1.5456tet')=='0'

def test_test_htmlfield():
    
    field=corefields.HTMLField('html')
    
    assert field.check('<p>"trial"</p><script></script>')=='"trial"'

    field.escape=True
    
    assert field.check('<p>"trial"</p><script></script>')=='&quot;trial&quot;'

    field.trusted_tags=['p']
    
    assert field.check('<p>"trial"</p><script></script>')=='<p>&quot;trial&quot;</p>'

    #field.
def test_test_foreignkeyfield():
    
    field=corefields.ForeignKeyField('foreign', ExampleModel())
    
    assert field.check('dsd')=='NULL'

def test_test_booleanfield():
    
    field=corefields.BooleanField('bool')
    
    assert field.check('25')=='0'
    
    assert field.error==True
    
    
def test_test_arrayfield():
    
    field=ArrayField('array', corefields.IntegerField('item'))
    
    assert field.check([1, 2, 3])=='["1", "2", "3"]'
    
    assert field.check([1, 2, '3t'])=='["1", "2", "0"]'
    
    assert field.error==True

def test_test_colorfield():
    
    field=ColorField('color')
    
    assert field.check('#ff12f5')==0xff12f5
    
    assert field.check('#ff12f54')==0
    
    assert field.get_hex_color(0xaf42f5)=='#af42f5'
    
def test_test_datefield():
    
    datetime.timezone='Europe/Madrid'
        
    datetime.set_timezone()
    
    field=DateField('date')
    
    field.utc=False
    
    assert field.check('20201234121011')==''
    
    assert field.check('20201230121011')=='20201230121011'
    
def test_test_datetimefield():
    
    datetime.timezone='Europe/Madrid'
        
    datetime.set_timezone()
    
    field=DateTimeField('date')
    
    field.utc=False
    
    assert field.check('20201230121011')=='2020-12-30 12:10:11'
    
    assert field.check('20201290121011')==''

def test_test_dictfield():
    
    field=DictField('dict', corefields.IntegerField('item'))
    
    assert field.check({'come': '5'})=='{"come": "5"}'
    
    assert field.check({'come': '5t'})=='{"come": "0"}'

    assert field.error==True

def test_test_emailfield():
    
    field=EmailField('email')
    
    assert field.check('example@example.com')=='example@example.com'
    
    assert field.check('example-example.com')==''
    
def test_test_i18nfield():
    
    
    
    pass
    
