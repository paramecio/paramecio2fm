from flask import Flask, session, url_for, request, send_file, abort
from settings import config
from importlib import import_module
import os
import sys
import inspect
from paramecio2.libraries.datetime import set_timezone

def start_app():
    
    set_timezone()
    
    app=Flask(__name__, static_url_path=config.static_url_path, static_folder=config.static_folder)

    app.secret_key=config.secret_key

    try:
        app.json.sort_keys=False

    except:
        
        app.config['JSON_SORT_KEYS']=False

    application_root='/'

    if hasattr(config, 'application_root'):
        #print(config.application_root)
        application_root=config.application_root

    
    app.config.update(
         APPLICATION_ROOT=application_root
    )
    
    #app.config['APPLICATION_ROOT']=application_root

    if hasattr(config, 'json_sort_keys'):
        app.config.update(
            JSON_SORT_KEYS=config.json_sort_keys
        )
        
    if hasattr(config, 'server_name'):
        app.config.update(
            SERVER_NAME=config.server_name
        )

    workdir=os.getcwd()
    arr_module_path={}

    # Load blueprints
    
    for key_app, added_app in config.apps.items():

        controller_path=import_module(added_app[0])
        
        controller_base=os.path.dirname(controller_path.__file__)
        
        dir_controllers=os.listdir(controller_base)
        
        app_name=getattr(controller_path, added_app[1])
        
        for controller in dir_controllers:
                
            if controller.find('.py')!=-1 and controller.find('__init__')==-1:
                    
                controller_py=controller.replace('.py', '')
                
                module_app=added_app[0]+'.'+controller_py
                
                a=import_module(module_app)
                
                arr_module_path[key_app]=os.path.dirname(sys.modules[module_app].__file__)
                
        app.register_blueprint(app_name, url_prefix=added_app[2])

    """
        module_app=config.apps[added_app][0]
        
        a=import_module(module_app)
        #print(added_app, file=sys.stdout)
        app_name=getattr(a, config.apps[added_app][1])

        app.register_blueprint(app_name, url_prefix=config.apps[added_app][2])
        
        arr_module_path[added_app]=os.path.dirname(sys.modules[module_app].__file__)
    """

    #Add media files support. Only for development. 
    
    @app.route('/mediafrom/<module>/<path:media_file>')
    def send_media_file(module, media_file):
        
        file_path=workdir+'/themes/'+config.theme+'/media/'+module+'/'+media_file
    
        if not os.path.isfile(file_path):
            #file_path=workdir+'/modules/'+module+'/media/'+media_file
            file_path=arr_module_path[module]+'/media/'+media_file

            if not os.path.isfile(file_path):
                abort(404)
                
        return send_file(file_path)

    return app
