from slugify import slugify as slugify_func

def slugify(slug, *args, **wargs):
    """Simple wrapper for slugify module https://github.com/un33k/python-slugify
    
    Args:
        slug (str): The string to be slugified
    
    """

    return slugify_func(slug, *args, **wargs)
