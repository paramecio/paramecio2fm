#!/usr/bin/env python3

from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db.coreforms import PasswordForm
from paramecio2.libraries.i18n import I18n
from flask import request

class UserModel(WebModel):
    """Model used with basic things for users login and signup
    """
    
    def __init__(self, name_field_id="id"):
        """
        Attributes:
            password_field (str): The name of the password field to check
            email_field (str): The name of the email field to check
            username_field (str): The name of the username field to check
            yes_repeat_password (bool): If True, check password repeat field, if False, no check repeat password field
            check_user (bool): If True, check if user exists in db table, if False, no check that.
            check_email (bool): If True, check if email exists in db table, if False, no check that.
        """
        
        super().__init__(name_field_id) 
        
        self.password_field='password'
        self.email_field='email'
        self.username_field='username'
        self.yes_repeat_password=True
        self.check_user=True
        self.check_email=True
    
    def create_forms(self, arr_fields=[]):
        """Method for create forms with checking of repeat password
        
        Args:
            arr_fields (list): List of fields used for generate the forms list
        """
        
        # Add password_repeat to forms from the model
        
        arr_fields=super().create_forms(arr_fields)
        
        if self.password_field in arr_fields and self.yes_repeat_password:
            
            repeat_password=PasswordForm('repeat_password', '')
    
            repeat_password.required=1
            
            repeat_password.label=I18n.lang('common', 'repeat_password', 'Repeat Password')
            
            repeat_password.field=self.fields[self.password_field]
            
            self.create_form_after(self.password_field, repeat_password)
        
        return arr_fields
            
    """
    def insert(self, dict_values, external_agent=True):
        
        if 'password' in dict_values:
            
            dict_values['repeat_password']=dict_values.get('repeat_password', '')
            
            if dict_values['repeat_password']!=dict_values[self.password_field]:
                self.fields[self.password_field].error=True
                self.fields['password'].txt_error=I18n.lang('common', 'error_passwords_no_match', 'Error: passwords doesn\'t match')
            
            return super().insert(dict_values, external_agent)
    """
    
    def check_all_fields(self, dict_values, external_agent, yes_update=False, errors_set="insert"):
        """Method for check all fields of a model for insert or update a row in table db. Special for UserModel class.
        
        Args:
            dict_values (dict): The dict of values to check
            external_agent (bool): If True, the query is considered manipulated by external agent and the checks are stricts, if not, checks are not stricts
            yes_update (bool): If True, the check need be done for update sql sentence, if False, the check is done for insert sql sentence
            errors_set (str): If insert value, the errors are set for insert sql statement, if update value, then the errors are set for update sql statement.
        """
        error=0
        
        try:
            
            fields, values, update_values=super().check_all_fields(dict_values, external_agent, yes_update, errors_set)
        except: 
            
            error+=1
        
        if self.check_user==True:
        
            # Check if passwords matches
            
            if self.password_field in dict_values:
                
                dict_values['repeat_password']=dict_values.get('repeat_password', '')
                
                if dict_values[self.password_field].strip()!="":
                
                    if dict_values['repeat_password']!=dict_values[self.password_field]:
                        
                        self.fields[self.password_field].error=True
                        self.fields[self.password_field].txt_error=I18n.lang('common', 'error_passwords_no_match', 'Error: passwords doesn\'t match')
                    
                        error+=1

            # Check if exists user with same email or password
            
            get_id=0
            
            if self.updated:
                # Need the id
                #GetPostFiles.obtain_get()
                #GetPostFiles.obtain_post()
                """
                getpostfiles=GetPostFiles()
                
                getpostfiles.obtain_get()
                """
                
                #get_id=getpostfiles.get.get(self.name_field_id, '0')
                get_id=request.args.get(self.name_field_id, '0')
                
                #post_id=getpostfiles.post.get(self.name_field_id, '0')
                post_id=request.form.get(self.name_field_id, '0')
                
                if get_id!='0':
                    get_id=int(get_id)
                
                if post_id!='0':
                    get_id=int(post_id)
                
                pass
            
            get_id=int(get_id)
            
            sql_id=''
            
            original_conditions=self.conditions
            
            self.reset_conditions()
            
            if self.username_field in dict_values:
            
                self.conditions=['WHERE '+self.username_field+'=%s AND '+self.name_field_id+'!=%s', [dict_values[self.username_field], get_id]]
                
            if self.select_count()>0:
                
                self.fields[self.username_field].error=True
                self.fields[self.username_field].txt_error=I18n.lang('common', 'error_username_exists', 'Error: username already exists')
                self.fields_errors[self.username_field].append(self.fields[self.username_field].txt_error) 
                error+=1

          
            if self.check_email:
            
                if self.email_field in dict_values:
                
                    self.conditions=['WHERE '+self.email_field+'=%s AND '+self.name_field_id+'!=%s', [dict_values[self.email_field], get_id]]            
                    
                if self.select_count()>0:
                    
                    self.fields[self.email_field].error=True
                    self.fields[self.email_field].txt_error=I18n.lang('common', 'error_email_exists', 'Error: this email is already being used')
                    self.fields_errors[self.email_field].append(self.fields[self.email_field].txt_error) 
                                    
                    error+=1
            
            self.conditions=original_conditions
        
        if error>0:
            self.query_error+='Error:if is not expected, please, check that you disabled the special checkings of this model'
            return False

        return fields, values, update_values
        
    
    
    
    
