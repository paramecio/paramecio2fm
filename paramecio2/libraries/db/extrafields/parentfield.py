#!/usr/bin/env python3

#from paramecio2.libraries.db.webmodel import PhangoField
from paramecio2.libraries.db.corefields import IntegerField
from paramecio2.libraries.db.coreforms import SelectModelForm
#from paramecio.citoplasma.httputils import GetPostFiles
from flask import request

class ParentField(IntegerField):
    """Field used for create fields used by save a parent id from a row in db."""
    
    def __init__(self, name, size=11, required=False, field_name='name'):
        """
        Args:
            name (str): The name of field
            size (int): The size of the new field in database. By default 11.
            required (bool): Boolean for define if field is required or not
            field_name (str): The name of the field used for identify the father row of the db.
        """
        
        super().__init__(name, size, required)
        
        #self.foreignkey=True
        self.indexed=True
        self.field_name=field_name

    def post_register(self):
        
        if self.model!=None:
            self.change_form(SelectModelForm, [self.model, self.field_name, self.model.name_field_id, self.name])
    
    def check(self, value):
        
        value=super().check(value)
        
        if self.model!=None:
            if self.model.updated==True:
                if self.model.name_field_id in self.model.post:
                    #GetPostFiles.obtain_get()

                    #model_id=GetPostFiles.get.get(self.model.name_field_id, '0')
                    
                    model_id=request.args.get(self.model.name_field_id, '0')
                    
                    if model_id==value:
                        self.error=True
                        self.txt_error='A field cannot be its own father'
                        self.required=True
                        value=0
                        return value
                        
                
        return value
