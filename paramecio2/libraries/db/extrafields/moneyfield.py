from paramecio2.libraries.db.corefields import DecimalField
from decimal import Decimal, getcontext
from locale import format_string

getcontext().prec=2

class MoneyField(DecimalField):
    """Field for maintain money values for e-commerce shopping for example."""
    
    def __init__(self, name, required=False):
        
        super().__init__(name, 11, required)

    def check(self, value):
        
        try:
            value=Decimal(value)
            
        except:
            value=0

        return value

    def show_formatted(self, value):
        
        return format_string('%.2f', Decimal(value), grouping=True)

    @staticmethod
    def format_money(value):
        return format_string('%.2f', Decimal(value), grouping=True)        
   

