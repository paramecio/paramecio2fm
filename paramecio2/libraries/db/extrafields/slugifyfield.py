#!/usr/bin/env python3

from paramecio2.libraries.db.corefields import CharField
from slugify import slugify
from paramecio2.libraries.db.coreforms import HiddenForm

class SlugifyField(CharField):
    """Field for process text and slugify it"""
    
    def __init__(self, name, size=255, field_related=None, required=False):
        
        super(SlugifyField, self).__init__(name, size, required)
        
        self.name_form=HiddenForm
        
        self.field_related=field_related
    
    def check(self, value):
        
        value=slugify(value)
        
        if value=='':
            
            if self.model!=None and self.field_related!=None:
                
                self.model.post[self.field_related]=self.model.post.get(self.field_related, '')
                
                value=slugify(self.model.post[self.field_related])
                
                if value=='':
            
                    self.error=True
                    self.error_txt='Value is empty'
                    
                    return ''
        return value
    

