from paramecio2.libraries.db.webmodel import WebModel, PhangoField
import json

class DictField(PhangoField):
    """Field for save json dicts with determined values"""
    
    def __init__(self, name, field_type, required=False):
        """
        Args:
            name (str): The name of field
            field_type (PhangoField): The type of PhangoField for save in ArrayField
            required (bool): Boolean for define if field is required or not
        """
        
        super().__init__(name, required)
        
        self.field_type=field_type
    
        self.error_default='Sorry, the json dict is invalid'

        self.set_default='NOT NULL'
    
    def check(self, value):
        
        if type(value).__name__=='str':
            try:
                value=json.loads(value)
            except json.JSONDecodeError:
                
                value={}
                self.error=True
                self.txt_error=self.error_default
                
        elif type(value).__name__!='dict':
            
            value={}
            self.error=True
            self.txt_error=self.error_default
        error=0
        for k,v in value.items():
            
            value[k]=self.field_type.check(v)
            if self.field_type.error:
                    error+=1
            
        final_value=json.dumps(value)
        
        if error>0:
            self.error=True
        
        #final_value=WebModel.escape_sql(final_value)
        
        return final_value

    def get_type_sql(self):

        return 'TEXT '+self.set_default
    
    def show_formatted(self, value):
        
        return ", ".join(value)
    
