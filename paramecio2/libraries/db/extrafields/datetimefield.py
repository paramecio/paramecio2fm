from paramecio2.libraries.db.corefields import PhangoField
from paramecio2.libraries import datetime
try:
    from paramecio2.libraries.db.extraforms.dateform import DateForm
except:
    class DateForm:
        pass

class DateTimeField(PhangoField):
    """Field for use and save dates in MySQL date format"""
    
    def __init__(self, name, size=255, required=False):
        
        super().__init__(name, size, required)
        
        self.name_form=DateForm
        
        self.utc=False
        
        self.error_default='Error: Date format invalid'        
    
    def check(self, value):
        
        if self.utc:
        
            value=datetime.local_to_gmt(value)
        
        elif not datetime.obtain_timestamp(value):
            
            self.error=True
            self.txt_error=self.error_default
            
            return '0000-00-00 00:00:00'
        
        if value==False:
            
            self.error=True
            self.txt_error=self.error_default
            return '0000-00-00 00:00:00'
        else:
            
            """
            format_date_txt="YYYY/MM/DD"

            format_time_txt="HH:mm:ss"
            """
            
            value=datetime.format_local_strtime('YYYY-MM-DD HH:mm:ss', value)
        
        return value
    
    def show_formatted(self, value):
        
        # Convert to paramecio value
        value=str(value)
        value=value.replace('-', '').replace(':', '').replace(' ', '')
        
        return datetime.format_date(value)

    def get_type_sql(self):
        
        """Method for return the sql code for this type

        """

        return 'DATETIME NOT NULL'
