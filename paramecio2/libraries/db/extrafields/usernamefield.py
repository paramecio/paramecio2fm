from paramecio2.libraries.db.corefields import PhangoField
from paramecio2.libraries.db.coreforms import PasswordForm
from hmac import compare_digest as compare_hash
import crypt
import re

class UserNameField(PhangoField):
    
    """Field for save and check usernames"""
    
    def check(self, value):
        
        if not re.match("^[A-Za-z0-9_-]+$", value):
            self.txt_error='Error: use only letters, numbers, underscores and dashes for this field'
            self.error=1
            value=''
            
        return value
