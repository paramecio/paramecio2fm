from paramecio2.libraries.db.webmodel import PhangoField,WebModel
import json

class ArrayField(PhangoField):
    """Field for save json arrays with determined values"""
    
    def __init__(self, name, field_type, required=False):
        """
        Args:
            name (str): The name of new field
            field_type (PhangoField): The type of PhangoField for save in ArrayField
            required (bool): Boolean for define if field is required or not
        """
        
        super().__init__(name, required)
        
        self.field_type=field_type
        
        self.error_default='Sorry, the json array is invalid'

        self.set_default='NOT NULL'
    
    def check(self, value):
        
        if type(value).__name__=='str':
            try:
                value=json.loads(value)
            except json.JSONDecodeError:
                
                value=[]
                self.error=True
                self.txt_error=self.error_default
                
        elif type(value).__name__!='list':
            
            value=[]
            self.error=True
            self.txt_error='Sorry, the json array is invalid'
        
        error=0
        
        if type(self.field_type).__name__!='ArrayField':        
            for k,v in enumerate(value):
                
                value[k]=self.field_type.check(v)
                if self.field_type.error:
                    error+=1
        
        if error>0:
            self.error=True
        
        final_value=json.dumps(value)
        
        final_value=WebModel.escape_sql(final_value)
        
        return final_value

    def get_type_sql(self):

        return 'TEXT '+self.set_default
    
    def show_formatted(self, value):
        
        return ", ".join(value)
    
    def loads(self, value):
        
        try:
        
            return json.loads(value)
        except:
            
            return False
