from paramecio2.libraries.db.corefields import PhangoField
from paramecio2.libraries import datetime
try:
    from paramecio2.libraries.db.extraforms.dateform import DateForm
except:
    
    class DateForm:
        pass

class DateField(PhangoField):
    """Field for use and save dates in YYYYMMDDHHSS format"""
    
    def __init__(self, name, size=255, required=False):
        
        super().__init__(name, size, required)
        
        self.name_form=DateForm
        
        self.utc=True
        
        self.error_default='Error: Date format invalid'        
    
    def check(self, value):
        
        if self.utc:
        
            value=datetime.local_to_gmt(value)
        
        elif not datetime.obtain_timestamp(value):

            self.error=True
            self.txt_error=self.error_default
            return ''
        
        if value==False:
            
            self.error=True
            self.txt_error=self.error_default
            return ''
        
        return value
    
    def show_formatted(self, value):
        
        return datetime.format_date(value)
