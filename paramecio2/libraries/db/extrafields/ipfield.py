from paramecio2.libraries.db.corefields import CharField
import ipaddress

class IpField(CharField):
    """Field for save ip internet address values in db"""
    
    def check(self, value):
        
        try:
        
            value=str(ipaddress.ip_address(value))
        
        except:
            
            self.error=True
            self.txt_error='No Valid IP'
            value=""

            
        return value
