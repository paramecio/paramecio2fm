#!/usr/bin/env python3

from paramecio2.libraries.db.coreforms import BaseForm
from paramecio2.libraries.i18n import I18n
from paramecio2.libraries.mtemplates import standard_t
import json

class I18nForm(BaseForm):
    """Form for data with multiple languages."""
    
    def __init__(self, name, value, form):
        """
        Args:
            name (str): The html name for this form
            value (str): The default value of this html form. 
            form (BaseForm): The form used for generate the multiple languade form. Example if you use a TextForm, a TextForm for every language will be showed.
        """
        
        super().__init__(name, value)
        
        self.form_child=form
        self.t=standard_t
    
    def form(self):
        
        lang_selected=I18n.get_default_lang()
        
        try:
            self.default_value=json.loads(self.default_value)
        except:
            self.default_value={}
        
        if type(self.default_value).__name__!='dict':
            self.default_value={}
        
        for lang in I18n.dict_i18n:
            self.default_value[lang]=self.default_value.get(lang, '')

        return standard_t.load_template('forms/i18nform.phtml', name_form=self.name_field_id, real_name_form=self.name, form=self.form_child, arr_i18n=I18n.dict_i18n, lang_selected=lang_selected, default_value=self.default_value)
