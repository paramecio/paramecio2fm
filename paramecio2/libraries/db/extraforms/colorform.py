#!/usr/bin/env python3

from paramecio2.libraries.db.coreforms import BaseForm
from paramecio2.libraries.mtemplates import standard_t

class ColorForm(BaseForm):
    """Form for get colors from a picker"""
    
    def __init__(self, name, value):
        
        super().__init__(name, value)
        
        self.t=standard_t
    
    def form(self):
        

        return self.t.load_template('forms/colorform.phtml', name_form=self.name_field_id, default_value=self.default_value, form=self)
