
from paramecio2.libraries.db.coreforms import BaseForm
from paramecio2.libraries.mtemplates import env_theme, PTemplate

env=env_theme(__file__)

t=PTemplate(env)

class TextHTMLForm(BaseForm):
    """Form for html texts, based in tinycme javascript library"""
    
    def __init__(self, name, value, t_add=None):
        """
        Args:
            name (str): The html name for this form
            value (str): The default value of this html form. 
            t_add (PTemplate): If you want change the standard html form, use other template loader
        """
        
        super().__init__(name, value)
        
        self.t=t_add
        
        if t_add==None:
            self.t=t
        
    def form(self):
        
        return self.t.load_template('forms/texthtmlform.phtml', form=self)
