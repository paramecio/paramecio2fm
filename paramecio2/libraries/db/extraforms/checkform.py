#!/usr/bin/env python3

from paramecio2.libraries.db.coreforms import BaseForm

class CheckForm(BaseForm):
    """Checkbox form, used normally with boolean fields"""
    
    def __init__(self, name, value, real_value=1):
        """
        Args:
            name (str): The html name for this form
            value (str): The default value of this html form. 
            real_value (str): Variable used for construct the html form, by default 1. 
        """
        
        super(CheckForm, self).__init__(name, value)
        
        self.real_value=real_value
    
    def form(self):
        
        arr_value={}
        
        arr_value[self.setform(self.default_value)]=''
        
        arr_value[self.setform(self.real_value)]='checked'
        
        return '<input type="checkbox" class="'+self.css+'" name="'+self.name+'" id="'+self.name_field_id+'" value="'+str(self.real_value)+'" '+arr_value[self.setform(self.default_value)]+'>'
