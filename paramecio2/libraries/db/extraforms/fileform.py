#!/usr/bin/env python3

from paramecio2.libraries.db.coreforms import BaseForm
from paramecio2.libraries.mtemplates import env_theme, PTemplate

env=env_theme(__file__)

t=PTemplate(env)

class FileForm(BaseForm):
    """Class for create a form for upload files. You shoud set enctype to True in your model for it"""

    def __init__(self, name, value, path):
        """
        Args:
            name (str): The html name for this form
            value (str): The default value of this html form. 
            path (str): The path where the file will be saved.
        """
        
        super().__init__(name, value)
        
        self.t=t
        self.enctype=True

    def form(self):
        
        return self.t.load_template('forms/fileform.phtml', form=self)
