"""
from settings import config

wsgi_gateway='flask'

if hasattr(config, 'wsgi_gateway'):
    wsgi_gateway=config.wsgi_gateway

if wsgi_gateway=='flask':
"""
from flask import request

def get_query_args(key, default_value=''):
    """Simple shortcuts for get query args from a http request
    
    A function that is used for have a shortcut for get query string args from a tipycal http request
    
    Args:
        key (str): The arg name or query key for extract from args array
        default_value (str): The default value if key is not set in args array
    
    """

    return request.args.get(key, default_value)
    
def get_post_args(key, default_value=''):
    """Simple shortcuts for get POST values from a http request
    
    A function that is used for have a shortcut for get POST values from a tipycal http POST request
    
    Args:
        key (str): The arg name or form key for extract from POST array
        default_value (str): The default value if key is not set in args array
    """
    
    return request.form.get(key, default_value)

