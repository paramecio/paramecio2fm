try:
    from settings import config
except:
    
    class config:
        
        application_root='/'
        domain_url='http://localhost/'
        yes_static=False
    
import urllib.parse

def make_url(path, query_args={}):
    
    """
        This is a method for create urls for the system
        
        Keyword arguments:
        path -- The path to the module
        query_args -- a ser of get variables for add to url
        
    """
    
    get_query=''
    
    if len(query_args)>0:
        
        get_query='?'+urllib.parse.urlencode(query_args)
    
    path=urllib.parse.quote_plus(path, safe='/')
    
    return config.application_root+path+get_query
    
def make_url_domain(path, query_args={}):

    """
        This is a method for create urls for the system, using the domain_url config variable how prefix
        
        Keyword arguments:
        path -- The path to the module
        query_args -- a ser of get variables for add to url
        
    """
    
    return config.domain_url+make_url(path, query_args)

def make_external_url(path, query_args={}):
    
    """
        This is a method for create urls for external systems
        
        Keyword arguments:
        path -- The base url of the url
        query_args -- a ser of get variables for add to url
        
    """
    
    get_query=''
    
    if len(query_args)>0:
        
        get_query='?'+urllib.parse.urlencode(query_args)
    
    return path+get_query

def add_get_parameters(url, **args):

    """
        This is a method for add args to existent url
        
        Keyword arguments:
        url -- The url
        args -- a ser of get variables for add to url
        
    """
    
    added_url='&'
    
    if url.find('?')==-1:
        added_url='?'
    
    get_query=urllib.parse.urlencode(args)
    
    return url+added_url+get_query


if config.yes_static==True:
        
    def make_media_url(file_path, module):

        """
            This is a method for create urls for media resources.
            
            Keyword arguments:
            file_path -- The relative path of module
            module -- the module where you can find the resource
        """
        
        return make_url('mediafrom/'+module+'/'+file_path) 
        #config.media_url+'mediafrom/'+module+'/'+file_path
else:
    
    def make_media_url(file_path, module):

        """
            This is a method for create urls for media resources if config.yes_static is disabled..
            
            Keyword arguments:
            file_path -- The relative path of module
            module -- the module where you can find the resource
            
        """
    
        return config.media_url+urllib.parse.quote_plus(module)+'/'+urllib.parse.quote_plus(file_path, safe='/')
