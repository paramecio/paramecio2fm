from settings import config
from paramecio2.libraries.urls import make_url, make_media_url, make_external_url
import time
import unittest

class TestUrlsMethods(unittest.TestCase):

    def test_urls(self):

        basic_url=make_url('welcome')
        
        self.assertEqual(basic_url, '/welcome')
        
        straigth_url=make_url('welcome', {'item1': 1, 'item2': 'accént'})
        
        self.assertEqual(straigth_url, '/welcome?item1=1&item2=acc%C3%A9nt')
        
        straigth_url=make_url('welcome', {'item1': 1, 'item2': 'thing with space'})
        
        self.assertEqual(straigth_url, '/welcome?item1=1&item2=thing+with+space')

    def test_external_urls(self):

        basic_url=make_external_url('http://coesinfo.com/welcome')
        
        self.assertEqual(basic_url, 'http://coesinfo.com/welcome')
        
        straigth_url=make_external_url('http://coesinfo.com/welcome', {'item1': 1, 'item2': 'accént'})
        
        self.assertEqual(straigth_url, 'http://coesinfo.com/welcome?item1=1&item2=acc%C3%A9nt')

        straigth_url=make_external_url('http://coesinfo.com/welcome', {'item1': 1, 'item2': 'thing with space'})
        
        self.assertEqual(straigth_url, 'http://coesinfo.com/welcome?item1=1&item2=thing+with+space')


    def test_media_urls(self):
        
        if config.yes_static:
        
            media_url=make_media_url('images/image.jpg', 'paramecio')
        
            self.assertEqual(media_url, config.media_url+'mediafrom/paramecio/images/image.jpg')
            
        else:

            media_url=make_media_url('images/image.jpg', 'paramecio')

            self.assertEqual(media_url, config.media_url+'media/paramecio/images/image.jpg')

if __name__ == '__main__':
    unittest.main()
    
    
    
